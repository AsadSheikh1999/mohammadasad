#daily coding problem 9

from typing import List

def solution(arr:List[int])->int:
    n=len(arr)
    #if array contains only one number
    if n==1:
        return arr[0]
    #if array contains two numbers
    if n==2:
        return max(arr)
    dp=[0]*n
    dp[0],dp[1]=arr[0], max(arr[0],arr[1])
    for i in range(2,n):
        dp[i]=max(dp[i-1],arr[i]+dp[i-2])
    return dp[-1]