#problem 7 by facebook

def solution(data: str)-> int:
    return helper(data, len(data))

def helper(data: str,k : int)->int:
    #if the length is zero ,we hafe reached the end of the string. Since it is empty, we return 1
    if k==0:
      return 1
    s=len(data)-k
    res=helper(data,k-1)
    #TO check if two digits before k are within the permissible range, k must be atleast. Hence we addthe condition 'k>=2' and check the number 'data[s:s+2]' is within[0,26].
    if k>=2 and int(data[s:s+2])<27:
        res +=helper(data,k-2)
    return res