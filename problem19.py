#problem19
def build_houses(matrix):
    k = len(matrix[0])
    soin_row = [0] * k

    for r,row in enumerate(matrix):
        new_row = []
        for c, val in enumerate(matrix):
            new_row.append(min(soin_row[i] for i in range(k) if i !=c) + val)
        soin_row = new_row
    return min(soin_row)