#problem 2
from typing import List
def solution(array:List[int])->List[int]:
    n,res=len(array), []
    for i in range(n):
        product =1
        for j in range(n):
             if i !=j:
                 product*=array[j]
        res.append(product)
    return res