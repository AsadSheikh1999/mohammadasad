#daily coding problem 8
#asked by Google

class Node:
    def __init__(self,val,left=None,right=None):
        self.val=val
        self.left=left
        self.right=right

def is_unival(root:Node)-> bool:
    #A null tree is also a universal tree
    if not root:
        return True
    
    #check the parent node's value with the left node
    if root.left and root.left.val !=root.val:
        return False

    #check the parent node's value with the right node
    if root.right and root.right.val !=root.val:
        return False
    return is_unival(root.left) and is_unival(root.right)

def solution(root:Node)->int:
    #Null check
    if not root:
        return 0
    
    #get unival trees from left and right
    total = count_univals(root.left)+count_univals(root.right)
    #if root node is unival tree add it to the count
    if is_unival(root):
        return 1+total
    return total